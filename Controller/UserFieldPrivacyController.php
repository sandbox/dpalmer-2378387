<?php
/**
 * @file
 * Contains \Drupal\UserFieldPrivacy\Controller\UserFieldPrivacyController;
 */

namespace Drupal\UserFieldPrivacy\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the User Field Privacy Settings form.
 */
class UserFieldPrivacyController extends ControllerBase {
  public function PrivacySettings() {
    $element = array(
      '#markup' => 'Hello, world',
    );
    return $element;
  }
}
